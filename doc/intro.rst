Introduction
============

...

.. _target_platform:

Target Platform
---------------

.. figure:: /_static/img/target-platform.svg
   :alt: Figure 1.

   Figure 1. Geteká's target platform


Although, theoretically, Geteká can run on any platform where Python can run,
the number of target platforms for this exercise has been reduced to the
platform illustrated in the diagram above. The reason for this reduction is
basically that the author does not support non-free platforms.


`Geteká`_
    A desktop application mini-template written in Python.

`Python`_
    A programming language, well integrated with the GNOME environment.

`GNOME Desktop Environment`_
    A desktop environment that provides system integration, user interface,
    multimedia, communication, data storage and other utility libraries for
    desktop applications.

`Debian-based Operating System`_
    Any libre operating system based on Debian.

Hardware
    Any computer where a Debian-based operating system can run.


See the final product
---------------------

The resulting product of this exercise is a *desktop application* that does
nothing in particular, but has basic integration with the GNOME desktop and
serves as a template to start building your own applications.

Install *Geteká* to see for yourself. Run the following command as superuser:

.. sourcecode:: console

   # pip install geteka

And that's about it. Now you can run *Geteká* in any of the following ways:

* Go to **GNOME Activities ▸ Applications ▸ Development** and launch *Geteká*.
* Press **Alt+F2**, write *geteka*, and press **Enter**.
* In a terminal, run ``$ geteka``.


.. _Debian-based Operating System: http://www.gnu.org/distros/free-distros.html
.. _Geteká: https://gitlab.com/luis-felipe/geteka
.. _GNOME Desktop Environment: http://www.gnome.org/
.. _Python: http://www.python.org/
