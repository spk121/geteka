Exercise Requirements
=====================

**Project Management**
    * Use a distributed version control system (DVCS) to manage the project
      source files.
    * Use a public hosting service for technology projects. The hosting service
      should provide, at least, the following items:

        * Integration with the distributed version control system (DVCS) of your
          choice.
        * Issue tracking. This is necessary to manage project tasks and bug
          reports.

**Programming Language**
    Use `Python`_ as the main programing language. The reasons for selecting
    this language are the following:

    * You already use it.
    * Easier to use than C, C++ or Vala.
    * Widely used in introductory courses to programming.
    * Available by default in most libre operating systems.
    * Well integrated with the GNOME platform.

**Graphical User Interface (GUI)**
    Since the final product's target platform includes the GNOME Desktop
    Environment:

    * Use `GTK+ 3`_ for the GUI. GTK+ 3 components can be used in Python
      programs through `PyGObject`_.
    * Use `Glade`_ to create the GUI definition in XML. Using Glade has the
      following advantages:

        * Faster GUI development.
        * Better separation between application presentation and business logic.
        * The GUI generated with glade is independent from the programming
          language, which means that the same GUI can be used without change
          in applications written in other languages such as C, C++, Vala, etc.

    * Use necessary files for integration with the `GNOME Desktop Environment`_.
    * Follow `GNOME Human Interface Guidelines`_.
    * Follow `GNOME Accessibility Guidelines`_.

**Software Architecture**
    Use a well known `software architectural pattern`_ for the application.

**User Manual**
    * Use `Mallard`_ for the application documentation (user manual). This is
      recommended, so that the documentation integrates with the
      `GNOME Help Framework`_.
    * Use `GNOME Documentation Style Guide`_.

**Development Process**
    * Use feature branches.
    * Provide `unit tests`_.
    * Provide development documentation using `Sphinx`_ and `RTD`_.

**Packaging and Distribution**
    * Use an appropriate project directory structure for
      `packaging and distributing the application as a Python package`_.
    * Publish the final product in the `Python Package Index`_ (PYPI).
    * Use of `pip package manager`_ as the recommended way of installing the
      final product.


.. _Debian-based Operating System: http://www.gnu.org/distros/free-distros.html
.. _Geteká: https://gitlab.com/luis-felipe/geteka
.. _Glade: https://glade.gnome.org/
.. _GNOME Accessibility Guidelines: https://developer.gnome.org/accessibility-devel-guide/stable/
.. _GNOME Desktop Environment: http://www.gnome.org/
.. _GNOME Documentation Style Guide: https://developer.gnome.org/gdp-style-guide/stable/
.. _GNOME Help Framework: https://projects.gnome.org/yelp/
.. _GNOME Human Interface Guidelines: https://developer.gnome.org/hig-book/
.. _GTK+ 3: http://www.gtk.org/
.. _Mallard: http://projectmallard.org/
.. _packaging and distributing the application as a Python package: http://guide.python-distribute.org/
.. _pip package manager: http://www.pip-installer.org/
.. _PyGObject: http://wiki.gnome.org/PyGObject
.. _Python: http://www.python.org/
.. _Python Package Index: https://pypi.python.org/pypi
.. _RTD: https://readthedocs.org/
.. _software architectural pattern: http://en.wikipedia.org/wiki/Model-View-Controller
.. _Sphinx: http://sphinx-doc.org/
.. _unit tests: http://docs.python.org/2/library/unittest.html#module-unittest
