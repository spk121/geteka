==================
Geteká User Manual
==================

The files contained in this directory are part of Geteká's User Manual.

This documentation is written in `Mallard`_, which is the standard format
used by the `GNOME Help Framework`_.


Reading the documentation
=========================

The User Manual can be accessed by end-users from the application's
primary menu. Developers can launch the manual by running the
following command::

  $ yelp "file://ABSOLUTE_PATH_TO_DIRECTORY/C/"


Modifying the documentation
===========================

The source files of the User Manual are XML-based and are located in
the C directory. You can use any text editor to modify them.





.. LINKS
.. _Mallard: http://projectmallard.org/
.. _GNOME Help Framework: https://projects.gnome.org/yelp/
