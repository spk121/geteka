Geteká
======

*Geteká* is a GTK+ desktop application exercise. The application
itself does nothing useful, it is just a set of files with a structure
that serve as a starting point to develop desktop applications that
integrate well with the GNOME Desktop Environment, and possibly with
other desktop environments that use Freedesktop.org conventions.


.. toctree::
   :maxdepth: 2
   :caption: Contents

   intro
   exercise-requirements
   project-structure
   development-environment
   development-process
   deployment


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
