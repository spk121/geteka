#!/usr/bin/env python3

"""Geteká Desktop Application.

GTK+ desktop application template.

"""

import sys

from geteka.gui import App


# Run the app
if __name__ == "__main__":
    app = App()
    exit_status = app.run(sys.argv)
    sys.exit(exit_status)
