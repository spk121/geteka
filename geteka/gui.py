"""Module for GUI components.

This module defines components for the Graphical User Interface (GUI)
of the application.

"""

import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gio, Gtk


# CLASSES
# =======

class App(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self,
                                 application_id="persona.luflac.geteka",
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.ui = Gtk.Builder.new_from_file("assets/app.ui")
        self.menus = Gtk.Builder.new_from_file("assets/menus.ui")

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Set application actions
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("help", None)
        action.connect("activate", self.on_help)
        self.add_action(action)
        self.set_accels_for_action("app.help", ["F1"])

        action = Gio.SimpleAction.new("preferences", None)
        action.connect("activate", self.on_preferences)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])

        # Set primary menu
        popover = Gtk.Popover.new_from_model(
            None,
            self.menus.get_object("primary_menu")
        )
        menu_btn = self.ui.get_object("primary_menu_button")
        menu_btn.set_popover(popover)

    def do_activate(self):
        window = self.ui.get_object("app_window")
        self.add_window(window)
        window.show_all()

        # Set window actions
        action = Gio.SimpleAction.new("say-hello", None)
        action.connect("activate", self.on_say_hello)
        window.add_action(action)

        action = Gio.SimpleAction.new("say-bye", None)
        action.connect("activate", self.on_say_bye)
        window.add_action(action)

        action = Gio.SimpleAction.new("revert", None)
        action.connect("activate", self.on_revert)
        window.add_action(action)

    def on_about(self, action, param):
        """Show application About dialog."""
        dialog = self.ui.get_object("about_dialog")
        dialog.run()
        dialog.destroy()

    def on_help(self, action, param):
        """Show application manual in Yelp."""
        window = self.get_active_window()
        screen = window.get_screen()
        Gtk.show_uri(screen, "help:geteka", Gtk.get_current_event_time())

    def on_preferences(self, action, param):
        """Show application preferences dialog."""
        # TODO: In a real App, display real preferences.
        #
        # See the Building applications section in the GTK+ 3
        # reference manual for an example of such dialog, and the
        # GSettings on the GIO reference manual for an API to store
        # and retrieve application settings.
        dialog = self.ui.get_object("missing_feature_dialog")
        dialog.run()
        dialog.destroy()

    def on_quit(self, action, param):
        """Show Application preferences dialog."""
        self.quit()

    def on_revert(self, action, param):
        """Revert the text view to its default text."""
        text_buffer = self.ui.get_object("text_buffer")
        text_view = self.ui.get_object("text_view")
        text_view.set_buffer(text_buffer)

    def on_say_bye(self, action, param):
        """Write Bye! on the text view."""
        text_buffer = self.ui.get_object("text_buffer_bye")
        text_view = self.ui.get_object("text_view")
        text_view.set_buffer(text_buffer)

    def on_say_hello(self, action, param):
        """Write Hello! on the text view."""
        text_buffer = self.ui.get_object("text_buffer_hello")
        text_view = self.ui.get_object("text_view")
        text_view.set_buffer(text_buffer)
