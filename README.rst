======
Geteká
======

*Geteká* is a GTK+ desktop application exercise. The application
itself does nothing useful, it is just a set of files with a structure
that serve as a starting point to develop desktop applications that
integrate well with the GNOME Desktop Environment, and possibly with
other desktop environments that use Freedesktop.org conventions.

More information in the `online documentation`_.



Install Requirements
====================

Create a Guix profile to install the packages required to develop and
run *Geteká*::

  $ entorno create geteka ~/path/to/geteka/requirements/guix.scm

Activate the profile::

  $ entorno workon --pure geteka

Export some environment variables::

  $ source .envrc



Run the Application
===================

Try out the application by running this command::

  $ cd path/to/geteka
  $ python3 run



References
==========

GNOME:

+ http://www.gnome.org/
+ http://developer.gnome.org/
+ https://gitlab.gnome.org/GNOME/libhandy/

Python:

+ http://www.python.org/
+ https://pygobject.readthedocs.io/





.. LINKS

.. _online documentation: https://luis-felipe.gitlab.io/geteka/
