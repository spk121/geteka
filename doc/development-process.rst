Development Process
===================

...


Starting Work on a New Version
------------------------------

* Create a new branch named after the version number (x.y.z).
* Update version number in the source code and documentation.
    * ``docs/source/conf.py``.
    * ``CHANGES``.
    * ``README``.
    * ``README.text``.
    * ``setup.py``.
* Update dependencies.
    * ``packages-debian``.
    * ``packages-python``.
    * ``setup.py``.
* Update `classifiers`_ in the ``setup.py`` file.


Finishing Work on a New Version
-------------------------------

* Update CHANGES.
* Merge new version branch to the master branch.


.. _classifiers: https://pypi.python.org/pypi?%3Aaction=list_classifiers
