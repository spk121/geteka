Deployment
==========

After the new version branch has been merged to the repository's default branch,
it's time to package the application and distribute it to the public Web sites
from where the final users can download and install automatically using the
`pip package manager`_.


Packaging the application
-------------------------

Run the following command from the project root directory:

.. sourcecode:: console

   $ python setup.py sdist

If everything goes right, a ``dist`` directory will be created and it will
contain the packaged application ready for distribution. The package name
should look like ``geteka-x.y.z.tar.gz``, where x, y and z should match the
version that will be released.

If you see the following warning while packaging the application, don't worry.
The MANIFEST file will be created automatically:

.. sourcecode:: console

   warning: sdist: manifest template 'MANIFEST.in' does not exist (using default file list)


Testing the package
-------------------

Before releasing the new version package to the world, install it locally using
the package created above, try out the application, and uninstall. Make sure
everything works as expected.

To install the application from the distribution package, run the following
command in the root directory of the project:

.. sourcecode:: console

   # pip install ./dist/geteka-x.y.z.tar.gz

Once the application is installed, run it. You should see the application runs
correctly:

.. sourcecode:: console

   $ geteka

Finally, uninstall:

.. sourcecode:: console

   # pip uninstall geteka

If no bugs were found during the testing, continue with the following section.
Otherwise, report any bugs to the `issue tracker`_, fix them, and start the
deployment steps from the beginning.


Distributing the application
----------------------------

Now that the new version of the application is packaged and tested, it's time
to upload it to public Web sites for distribution.

Upload to PYPI
~~~~~~~~~~~~~~

This is the most important place to upload to since the
`Python Package Index`_ (PYPI) is the standard repository for distributing
third-party packages for Python.

If this is the first release of the application, you will have to register the
package first in PYPI. To do so, run the following command from the project root
directory:

.. sourcecode:: console

   $ python setup.py register

Once the package has been registered you will see that a project page has been
created in PYPI for geteka: https://pypi.python.org/pypi/geteka.

Now, upload the new package version by running the following command in the
project root directory:

.. sourcecode:: console

   $ python setup.py sdist upload

Visit the project page in PYPI to see the changes.

Upload to Bitbucket
~~~~~~~~~~~~~~~~~~~

Since the project source is hosted in Bitbucket, it's a good idea to add the
new release to the *Downloads* section.

Go to the `Downloads section`_ and upload the application package located in the
``dist`` directory.


Release Announcement
--------------------

At this point, you can go ahead and announce the package release in your Web
site, Blog, user base mailing list, etc. You can use
**Appendix A: Release Announcement Template**.


.. _pip package manager: http://www.pip-installer.org/
.. _issue tracker: https://bitbucket.org/sirgazil/geteka/issues/
.. _Downloads section: https://bitbucket.org/sirgazil/geteka/downloads/
.. _Python Package Index: https://pypi.python.org/pypi
