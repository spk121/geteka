#| Geteká Desktop Application (Guile Scheme version). |#

(use-modules (gi)
             (gi repository))

(require "Gio" "2.0")
(require "Gtk" "3.0")

(load-by-name "Gio" "ActionMap")  ; add-action
(load-by-name "Gio" "Application")  ; activate, run
(load-by-name "Gio" "SimpleAction")
(load-by-name "Gtk" "Application")
(load-by-name "Gtk" "ApplicationWindow")
(load-by-name "Gtk" "Builder")
(load-by-name "Gtk" "init")
(load-by-name "Gtk" "MenuButton")  ; set-popover
(load-by-name "Gtk" "Popover")
(load-by-name "Gtk" "Widget")  ; show-all


;;; INITIALIZE GTK
;;; ==============

(init!)



;;; CONSTANTS
;;; =========

(define %app-id "persona.luflac.geteka")

(define %app-ui-file "assets/app.ui")
(define %menus-ui-file "assets/menus.ui")

(define %ui-builder (builder:new-from-file %app-ui-file))
(define %menu-builder (builder:new-from-file %menus-ui-file))



;;; CALLBACKS
;;; =========

(define (app:activate app)
  (let [(window (builder:get-object %ui-builder "app_window"))
        (action:say-hello (make <GSimpleAction> #:name "say-hello"))
        (action:say-bye (make <GSimpleAction> #:name "say-bye"))
        (action:revert (make <GSimpleAction> #:name "revert"))]

    ;; Add window to application.
    (add-window app window)

    ;; Connect window actions.
    (connect action:say-hello activate say-hello)
    (connect action:say-bye activate say-bye)
    (connect action:revert activate revert)

    ;; Add actions to window.
    (add-action window action:say-hello)
    (add-action window action:say-bye)
    (add-action window action:revert)

    ;; Show the window and everything inside it.
    (show-all window)))


(define (app:startup app)
  (let* [(primary-menu (builder:get-object %menu-builder "primary_menu"))
         (primary-menu-btn (builder:get-object %ui-builder "primary_menu_button"))
         (popover (popover:new-from-model primary-menu-btn primary-menu))

         (action:about (make <GSimpleAction> #:name "about"))
         (action:help (make <GSimpleAction> #:name "help"))
         (action:preferences (make <GSimpleAction> #:name "preferences"))
         (action:quit (make <GSimpleAction> #:name "quit"))]

    ;; Connect application actions.
    (connect action:about activate show-about-dialog)
    (connect action:help activate show-help)
    (connect action:preferences activate show-preferences)
    (connect action:quit activate quit-app)

    ;; Define keyboard shortcuts for actions.
    (set-accels-for-action app "app.help" (list "F1"))
    (set-accels-for-action app "app.quit" (list "<Ctrl>Q"))

    ;; Add actions to app.
    (add-action app action:about)
    (add-action app action:help)
    (add-action app action:preferences)
    (add-action app action:quit)

    ;; Set primary menu.
    (set-popover primary-menu-btn popover)))


(define (print-hello widget)
  #| Print a greeting on the terminal. |#
  (display "Hello World\n"))


(define (quit-app action param)
  #| Quit the application. |#
  (display "TODO: Quit application\n"))


(define (revert action param)
  #| Revert the text view to its default text. |#
  (let [(text-buffer (builder:get-object %ui-builder "text_buffer"))
        (text-view (builder:get-object %ui-builder "text_view"))]

    (set-buffer text-view text-buffer)))


(define (say-bye action param)
  #| Write a good bye on the text view. |#
  (let [(text-buffer (builder:get-object %ui-builder "text_buffer_bye"))
        (text-view (builder:get-object %ui-builder "text_view"))]

    (set-buffer text-view text-buffer)))


(define (say-hello action param)
  #| Write a greeting on the text view. |#
  (let [(text-buffer (builder:get-object %ui-builder "text_buffer_hello"))
        (text-view (builder:get-object %ui-builder "text_view"))]

    (set-buffer text-view text-buffer)))


(define (show-about-dialog action param)
  (display "TODO: Show About dialog\n"))


(define (show-help action param)
  (display "TODO: Show application manual\n"))


(define (show-preferences action param)
  (display "TODO: Show application preferences\n"))



;;; RUN APP
;;; =======

(let [(app (make <GtkApplication> #:application-id %app-id))]

  (connect app startup app:startup)
  (connect app activate app:activate)
  (exit (run app (command-line))))
