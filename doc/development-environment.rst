Development Environment
=======================

*Geteká* is developed on the same platform described in the
:ref:`target_platform` page, with the addition of some other
development tools:


`Glade`_
    A Rapid Application Development (RAD) tool that helps speed up the
    development of user interfaces for the GTK+ toolkit and the GNOME
    desktop environment.

    Glade is used to design Geteká's graphical user interface and
    save it as an XML description.

`pip`_
    A tool for installing and managing Python packages.

    pip is the recommended package manager to install Geteká
    itself. It is also used to install some Geteká's dependencies.

`PyGObject`_
    A Python dynamic module that enables developers to use GObject,
    which is an important part of the GNOME platform.

    PyGObject is used by Geteká to use the GTK+ 3 user interface
    components.

`Sphinx`_
    A tool that makes it easy to create intelligent and beautiful
    documentation.

    Sphinx is used to write Geteká's development documentation.


.. REFERENCES
.. _Glade: https://glade.gnome.org/
.. _pip: http://www.pip-installer.org/en/latest/
.. _PyGObject: https://wiki.gnome.org/PyGObject
.. _Sphinx: http://sphinx.pocoo.org/
