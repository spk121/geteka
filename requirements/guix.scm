#| GNU Guix manifest

Packages required to develop the project.

This file is a GNU Guix manifest file. It can be used with the Guix
package command to create a profile with the packages listed here. |#

(use-modules (gnu packages))


(define %base-packages
  (list "coreutils"
        "gettext"
        "git"
        "gobject-introspection"
        "gtk+"
        "libhandy"))

(define %python-packages
  (list "python"
        "python-pygobject"
        "python-sphinx"
        "python-sphinx-rtd-theme"))

(define %guile-packages
  (list "guile"
        "guile-gi"))


(specifications->manifest
 (append %base-packages
         %python-packages
         %guile-packages))
